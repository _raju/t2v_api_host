<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientOauthTokenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_oauth_token', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('client_master_id')->default('0');
            $table->text('oauth_token');
            $table->integer('oauth_token_expire_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_oauth_token');
    }
}
