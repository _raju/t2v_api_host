<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_master', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('client_name', 50);
            $table->string('client_id', 100);
            $table->string('client_secret_key', 100);
            $table->string('client_grant_type', 50);
            $table->string('encryption_key');
            $table->tinyInteger('status')->default('0')->comment('0=Inactive, 1=Active');
            $table->timestamps();
            $table->integer('created_by')->default('0');
            $table->integer('updated_by')->default('0');
        });

        // Insert data
        DB::table('client_master')->insert(
            array(
                'client_name' => 'Tech2View',
                'client_id' => 'tech_2_view_2021-@',
                'client_secret_key' => '5b1b5d1f0cea2a77e1df2bf195883eb8',
                'client_grant_type' => 'tech_2_view_credentials',
                'encryption_key' => 'tech2view2021#%@',
                'status' => 1,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'created_by' => 0,
                'updated_by' => 0
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_master');
    }
}
