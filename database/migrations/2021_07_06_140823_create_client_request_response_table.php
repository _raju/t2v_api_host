<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientRequestResponseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_request_response', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('request_type', 100)->nullable();
            $table->integer('client_master_id');
            $table->integer('client_oauth_token_id');
            $table->text('request_json');
            $table->text('response_json');
            $table->integer('user_id')->default('0')->nullable();
            $table->integer('status_code')->default('0')->comment('200 = success , , 400 = bad request, 401 = unauthorized, 500 = internal server error');
            $table->string('ip_address', 100)->nullable();
            $table->string('user_agent', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_request_response');
    }
}
