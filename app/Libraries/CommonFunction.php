<?php


namespace App\Libraries;


use App\Modules\API\Models\ClientOauthToken;
use App\Modules\API\Service\ApiTokenServiceJwt;
use App\Modules\API\Service\Tech2ViewAppApi;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class CommonFunction
{
    use ApiTokenServiceJwt, Tech2ViewAppApi;

    /**
     * token common validity check
     *
     * @param $request_type
     * @param $token
     * @param $request_data
     *
     * @return array
     */
    public static function tokenCommonValidityCheck($request_type, $token, $request_data)
    {
        return (new self())->tokenValidityCheck($request_type, $token, $request_data);
    }

    /**
     * token common validity check
     *
     * @param $request_type
     * @param $token
     * @param $request_data
     *
     * @return array
     */
    public function tokenValidityCheck($request_type, $token, $request_data)
    {
        try {

            // check whether bearer token exists in the request header
            if (empty($token)) {
                $response_data = $this->dataResponse('Token not found in request header!', 'TECH-2-VIEW-' . $request_type, HTTPResponse::HTTP_BAD_REQUEST);
                $this->clientRequestResponse($request_type, 0, 0, json_encode($request_data), json_encode($response_data), 0, HTTPResponse::HTTP_BAD_REQUEST);

                return ['success' => false, 'data_response' => response()->json($response_data)];
            }

            $clientData = ClientOauthToken::leftJoin('client_master', 'client_master.id', '=', 'client_oauth_token.client_master_id')
                ->where('client_oauth_token.oauth_token', $token)
                ->first([
                    'client_master.id as client_master_id',
                    'client_master.client_id',
                    'client_master.client_secret_key',
                    'client_oauth_token.id as client_oauth_token_id',
                    'client_oauth_token.oauth_token_expire_at'
                ]);

            // invalid token check
            if (empty($clientData)) {
                $response_data = $this->dataResponse('Invalid token!', 'TECH-2-VIEW-' . $request_type, HTTPResponse::HTTP_BAD_REQUEST);
                $this->clientRequestResponse($request_type, 0, 0, json_encode($request_data), json_encode($response_data), 0, HTTPResponse::HTTP_BAD_REQUEST);

                return ['success' => false, 'data_response' => response()->json($response_data)];
            }

            // token expiration check
            if (date('Y-m-d H:i:s') > $clientData->oauth_token_expire_at) {
                $response_data = $this->dataResponse('Client Token has been expired!', 'TECH-2-VIEW-' . $request_type, HTTPResponse::HTTP_UNAUTHORIZED);
                $this->clientRequestResponse($request_type, $clientData->client_master_id, $clientData->client_oauth_token_id, json_encode($request_data), json_encode($response_data), 0, HTTPResponse::HTTP_UNAUTHORIZED);

                return ['success' => false, 'data_response' => response()->json($response_data)];
            }

        } catch (\Exception $e) {
            $response_data = $this->dataResponse($e->getMessage(), 'TECH-2-VIEW-' . $request_type, HTTPResponse::HTTP_BAD_REQUEST);
            $this->clientRequestResponse($request_type, $clientData->client_master_id, $clientData->client_oauth_token_id, json_encode($request_data), json_encode($response_data), 0, HTTPResponse::HTTP_BAD_REQUEST);

            return ['success' => false, 'data_response' => response()->json($response_data)];
        }

        return ['success' => true, 'data_response' => $clientData];
    }
}
