<?php

namespace App\Modules\API\Models;

use Illuminate\Database\Eloquent\Model;

class ClientOauthToken extends Model
{
    protected $table = 'client_oauth_token';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'client_master_id',
        'oauth_token',
        'oauth_token_expire_at',
        'created_at',
        'updated_at'
    ];

}
