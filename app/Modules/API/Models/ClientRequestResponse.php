<?php

namespace App\Modules\API\Models;

use Illuminate\Database\Eloquent\Model;

class ClientRequestResponse extends Model
{

    protected $table = 'client_request_response';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'request_type',
        'client_master_id',
        'client_oauth_token_id',
        'request_json',
        'response_json',
        'user_id',
        'status_code',
        'ip_address',
        'user_agent',
        'created_at',
        'updated_at'
    ];

}
