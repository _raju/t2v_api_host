<?php


namespace App\Modules\API\Service;

use App\Modules\API\Models\ClientOauthToken;
use Firebase\JWT\JWT;

trait ApiTokenServiceJwt
{
    /**
     * @param $clientData
     * @return array|null
     */
    private function generateToken($clientData)
    {
        try {
            // date_default_timezone_set('Asia/Dhaka');
            $token_encode_data = [
                'client_grant_type' => $clientData->client_secret_key,
                'client_id' => $clientData->client_id,
                'client_secret_key' => $clientData->client_secret_key,
                "exp" => time() + 300
            ];
            $jwt_token = JWT::encode($token_encode_data, $clientData->encryption_key);

            $token_response['token_type'] = 'bearer';
            $token_response['token'] = $jwt_token;
            $token_response['expire_in'] = time() + 300; // 5 minutes increase

            return [
                'token_response' => $token_response,
                'client_oauth_token_id' => $this->storeToken($clientData->id, $token_response['token'], $token_response['expire_in'])
            ];

        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @param $client_master_id
     * @param $token
     * @param $token_expire_in
     *
     * @return client_oauth_token_id
     */
    private function storeToken($client_master_id, $token, $token_expire_in)
    {
        $client_oauth_token = new ClientOauthToken();
        $client_oauth_token->client_master_id = $client_master_id;
        $client_oauth_token->oauth_token = $token;
        $client_oauth_token->oauth_token_expire_at = $token_expire_in;
        $client_oauth_token->save();

        return $client_oauth_token->id;
    }

}
