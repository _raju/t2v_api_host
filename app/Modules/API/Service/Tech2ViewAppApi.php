<?php


namespace App\Modules\API\Service;


use App\Modules\API\Models\ClientRequestResponse;
use http\Client\Response;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

trait Tech2ViewAppApi
{
    /**
     * @param mixed
     *
     * @return void
     */
    private function clientRequestResponse($request_type, $client_master_id = 0, $client_oauth_token_id = 0, $request_json, $response_json, $user_id = 0, $status_code = Response::HTTP_UNAUTHORIZED)
    {
        $req_res = new ClientRequestResponse();
        $req_res->request_type = $request_type;
        $req_res->client_master_id = $client_master_id;
        $req_res->client_oauth_token_id = $client_oauth_token_id;
        $req_res->request_json = $request_json;
        $req_res->response_json = $response_json;
        $req_res->user_id = $user_id;
        $req_res->status_code = $status_code;
        $req_res->ip_address = \Request::getClientIp();
        $req_res->user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'N/A';
        $req_res->save();
    }

    /**
     * @param string $message
     * @param string $response_type
     * @param int $status_code
     * @param $data
     * @return array
     */
    private function dataResponse($message = '', $response_type, $status_code = HttpResponse::HTTP_BAD_REQUEST, $data = '')
    {
        $response['tech2ViewResponse'] = [
            'responseTime' => time(),
            'responseType' => $response_type,
            'responseCode' => $status_code,
            'responseData' => $data,
            'message' => $message
        ];

        return $response;
    }
}
