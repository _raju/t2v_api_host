<?php

Route::group(['prefix' => 'api/v1', 'module' => 'API', 'middleware' => ['api'], 'namespace' => 'App\Modules\API\Controllers'], function () {

    //token generation
    Route::post('get-token', 'ApiTokenController@getToken');

    //authentication
    Route::post('signup', 'ApiAuthenticateController@signup');
    Route::post('login', 'ApiAuthenticateController@login');

    //profile update
    Route::post('profile-update', 'ApiProfileController@updateProfile');
});
