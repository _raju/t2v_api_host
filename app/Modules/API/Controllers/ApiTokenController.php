<?php


namespace App\Modules\API\Controllers;


use App\Modules\API\Models\ApiClientMaster;
use App\Modules\API\Service\ApiTokenServiceJwt;
use App\Modules\API\Service\Tech2ViewAppApi;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class ApiTokenController
{
    use Tech2ViewAppApi, ApiTokenServiceJwt;

    /**
     * Token generate
     *
     * @param Request $request
     * @return Illuminate\Http\JsonResponse
     */
    public function getToken(Request $request)
    {
        try {

            $grant_type = trim($request->grant_type);
            $client_id = trim($request->client_id);
            $client_secret_key = trim($request->client_secret_key);
            $clientData = $this->checkClient($grant_type, $client_id, $client_secret_key);

            if (!$clientData) {
                $response_data = $this->dataResponse('Client information is not valid!', 'TECH-2-VIEW-API-TOKEN', HTTPResponse::HTTP_UNAUTHORIZED);
                $this->clientRequestResponse('TOKEN_REQUEST', 0, 0, json_encode($request->all()), json_encode($response_data), 0, HTTPResponse::HTTP_UNAUTHORIZED);

                return response()->json($response_data);
            }

            // JWT token generation
            $jwt_token_array = $this->generateToken($clientData);

            if ($jwt_token_array['token_response'] == null) {
                $response_data = $this->dataResponse('Token generation failed!', 'TECH-2-VIEW-API-TOKEN', HTTPResponse::HTTP_INTERNAL_SERVER_ERROR);
                $this->clientRequestResponse('TOKEN_REQUEST', $clientData->id, 0, json_encode($request->all()), json_encode($response_data), 0, HTTPResponse::HTTP_INTERNAL_SERVER_ERROR);

                return response()->json($response_data);
            }

            // Successfully generated token
            $response_data = $this->dataResponse('Successfully generated token.', 'TECH-2-VIEW-API-TOKEN', HTTPResponse::HTTP_OK, $jwt_token_array['token_response']);
            $this->clientRequestResponse('TOKEN_REQUEST', $clientData->id, $jwt_token_array['client_oauth_token_id'], json_encode($request->all()), json_encode($response_data), 0, HTTPResponse::HTTP_OK);

            return response()->json($response_data);

        } catch (\Exception $e) {
            $response_data = $this->dataResponse('Sorry! An internal error has occurred.', 'TECH-2-VIEW-API-TOKEN', HTTPResponse::HTTP_INTERNAL_SERVER_ERROR);
            $this->clientRequestResponse('TOKEN_REQUEST', 0, 0, json_encode($request->all()), json_encode($response_data), 0, HTTPResponse::HTTP_INTERNAL_SERVER_ERROR);

            return response()->json($response_data);
        }
    }

    /**
     * @param $client_id
     * @param $client_secret_key
     *
     * @return mixed
     */
    private function checkClient($grant_type, $client_id, $client_secret_key)
    {
        return ApiClientMaster::where([
            'client_grant_type' => $grant_type,
            'client_id' => $client_id,
            'client_secret_key' => $client_secret_key,
            'status' => 1
        ])
            ->first(['id', 'client_name', 'client_id', 'client_secret_key', 'client_grant_type', 'encryption_key']);
    }
}
