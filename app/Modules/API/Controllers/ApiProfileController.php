<?php


namespace App\Modules\API\Controllers;


use App\Libraries\CommonFunction;
use App\Modules\API\Service\ApiTokenServiceJwt;
use App\Modules\API\Service\Tech2ViewAppApi;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class ApiProfileController
{
    use Tech2ViewAppApi, ApiTokenServiceJwt;

    /**
     * Profile Update
     *
     * @param $requestData
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateProfile(Request $request)
    {
        $apache_request_headers = apache_request_headers();
        $bearer_token = isset($apache_request_headers['Authorization']) ? $apache_request_headers['Authorization'] : '';
        $token = str_ireplace("bearer ", "", $bearer_token);

        $request_data = [
            'ip' => \Request::getClientIp(),
            'token' => $token,
            'email' => $request->email,
            'name' => $request->name
        ];

        try {

            // token common validation check
            $token_validity = CommonFunction::tokenCommonValidityCheck('PROFILE_UPDATE', $token, $request_data);

            if (!$token_validity['success']) {
                return $token_validity['data_response'];
            }

            $clientData = $token_validity['data_response'];

            // input validation
            $rules = [
                'name' => 'required',
                'email' => 'required'
            ];

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $messages = [];
                foreach ($validator->messages()->getMessages() as $key => $value) {
                    $messages[] = ['field' => $key, 'error' => $value[0]];
                }

                $response_data = $this->dataResponse($messages, 'TECH-2-VIEW-PROFILE-UPDATE', HTTPResponse::HTTP_UNAUTHORIZED);
                $this->clientRequestResponse('PROFILE_UPDATE', $clientData->client_master_id, $clientData->client_oauth_token_id, json_encode($request_data), json_encode($response_data), 0, HTTPResponse::HTTP_UNAUTHORIZED);

                return response()->json($response_data);
            }

            $userInfo = User::where('email', $request->email)->first();
            if (!$userInfo) {
                // invalid user
                $response_data = $this->dataResponse('Invalid user!', 'TECH-2-VIEW-PROFILE-UPDATE', HTTPResponse::HTTP_UNAUTHORIZED);
                $this->clientRequestResponse('PROFILE_UPDATE', $clientData->client_master_id, $clientData->client_oauth_token_id, json_encode($request_data), json_encode($response_data), 0, HTTPResponse::HTTP_UNAUTHORIZED);

                return response()->json($response_data);
            }

            // update user info
            $userInfo->name = $request->name;
            $userInfo->save();

            $response_data = $this->dataResponse('Profile update successfully', 'TECH-2-VIEW-PROFILE-UPDATE', HTTPResponse::HTTP_OK, $request->all());
            $this->clientRequestResponse('PROFILE_UPDATE', $clientData->client_master_id, $clientData->client_oauth_token_id, json_encode($request_data), json_encode($response_data), $userInfo->id, HTTPResponse::HTTP_OK);

            return response()->json($response_data);

        } catch (\Exception $e) {
            $response_data = $this->dataResponse('Sorry! An internal error has occurred.', 'TECH-2-VIEW-PROFILE-UPDATE', HTTPResponse::HTTP_INTERNAL_SERVER_ERROR);
            $this->clientRequestResponse('PROFILE_UPDATE', 0, 0, json_encode($request->all()), json_encode($response_data), 0, HTTPResponse::HTTP_INTERNAL_SERVER_ERROR);

            return response()->json($response_data);
        }
    }

}
