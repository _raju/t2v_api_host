<?php

namespace App\Modules\API\Controllers;


use App\Libraries\CommonFunction;
use App\Modules\API\Service\ApiTokenServiceJwt;
use App\Modules\API\Service\Tech2ViewAppApi;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class ApiAuthenticateController extends Controller
{
    use Tech2ViewAppApi, ApiTokenServiceJwt;

    /**
     * Sign up
     *
     * @param $requestData
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function signup(Request $request)
    {
        $apache_request_headers = apache_request_headers();
        $bearer_token = isset($apache_request_headers['Authorization']) ? $apache_request_headers['Authorization'] : '';
        $token = str_ireplace("bearer ", "", $bearer_token);

        $request_data = [
            'ip' => \Request::getClientIp(),
            'token' => $token,
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => $request->get('password')
        ];

        try {

            // token common validation check
            $token_validity = CommonFunction::tokenCommonValidityCheck('USER_SIGN_UP', $token, $request_data);

            if (!$token_validity['success']) {
                return $token_validity['data_response'];
            }

            $clientData = $token_validity['data_response'];

            // input validation
            $rules = [
                'name' => 'required',
                'email' => 'required|unique:users',
                'password' => 'required'
            ];

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $messages = [];
                foreach ($validator->messages()->getMessages() as $key => $value) {
                    $messages[] = ['field' => $key, 'error' => $value[0]];
                }

                $response_data = $this->dataResponse($messages, 'TECH-2-VIEW-SIGN-UP', HTTPResponse::HTTP_UNAUTHORIZED);
                $this->clientRequestResponse('USER_SIGN_UP', $clientData->client_master_id, $clientData->client_oauth_token_id, json_encode($request_data), json_encode($response_data), 0, HTTPResponse::HTTP_UNAUTHORIZED);

                return response()->json($response_data);
            }

            // Insert new user
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->save();

            $response_data = $this->dataResponse('Successfully sign up', 'TECH-2-VIEW-SIGN-UP', HTTPResponse::HTTP_OK, $request->all());
            $this->clientRequestResponse('USER_SIGN_UP', $clientData->client_master_id, $clientData->client_oauth_token_id, json_encode($request_data), json_encode($response_data), $user->id, HTTPResponse::HTTP_OK);

            return response()->json($response_data);

        } catch (\Exception $e) {
            $response_data = $this->dataResponse('Sorry! An internal error has occurred.', 'TECH-2-VIEW-SIGN-UP', HTTPResponse::HTTP_INTERNAL_SERVER_ERROR);
            $this->clientRequestResponse('USER_SIGN_UP', 0, 0, json_encode($request->all()), json_encode($response_data), 0, HTTPResponse::HTTP_INTERNAL_SERVER_ERROR);

            return response()->json($response_data);
        }
    }

    /**
     * Login
     *
     * @param $requestData
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $apache_request_headers = apache_request_headers();
        $bearer_token = isset($apache_request_headers['Authorization']) ? $apache_request_headers['Authorization'] : '';
        $token = str_ireplace("bearer ", "", $bearer_token);

        $request_data = [
            'ip' => \Request::getClientIp(),
            'token' => $token,
            'email' => $request->email,
            'password' => $request->password
        ];

        try {

            // token common validation check
            $token_validity = CommonFunction::tokenCommonValidityCheck('USER_LOGIN', $token, $request_data);

            if (!$token_validity['success']) {
                return $token_validity['data_response'];
            }

            $clientData = $token_validity['data_response'];

            // input validation
            $rules = [
                'email' => 'required',
                'password' => 'required'
            ];

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $messages = [];
                foreach ($validator->messages()->getMessages() as $key => $value) {
                    $messages[] = ['field' => $key, 'error' => $value[0]];
                }

                $response_data = $this->dataResponse($messages, 'TECH-2-VIEW-LOGIN', HTTPResponse::HTTP_UNAUTHORIZED);
                $this->clientRequestResponse('USER_LOGIN', $clientData->client_master_id, $clientData->client_oauth_token_id, json_encode($request_data), json_encode($response_data), 0, HTTPResponse::HTTP_UNAUTHORIZED);

                return response()->json($response_data);
            }

            $userInfo = User::where('email', $request->email)->first();
            if ($userInfo) {
                if (Hash::check($request->password, $userInfo->password)) {
                    // valid user
                    $response_data = $this->dataResponse('Login successfully', 'TECH-2-VIEW-LOGIN', HTTPResponse::HTTP_OK, $request->all());
                    $this->clientRequestResponse('USER_LOGIN', $clientData->client_master_id, $clientData->client_oauth_token_id, json_encode($request_data), json_encode($response_data), $userInfo->id, HTTPResponse::HTTP_OK);

                    return response()->json($response_data);
                }
            }

            // invalid username/password
            $response_data = $this->dataResponse('Username or password is invalid!', 'TECH-2-VIEW-LOGIN', HTTPResponse::HTTP_UNAUTHORIZED);
            $this->clientRequestResponse('USER_LOGIN', $clientData->client_master_id, $clientData->client_oauth_token_id, json_encode($request_data), json_encode($response_data), 0, HTTPResponse::HTTP_UNAUTHORIZED);

            return response()->json($response_data);

        } catch (\Exception $e) {
            $response_data = $this->dataResponse('Sorry! An internal error has occurred.', 'TECH-2-VIEW-LOGIN', HTTPResponse::HTTP_INTERNAL_SERVER_ERROR);
            $this->clientRequestResponse('USER_LOGIN', 0, 0, json_encode($request->all()), json_encode($response_data), 0, HTTPResponse::HTTP_INTERNAL_SERVER_ERROR);

            return response()->json($response_data);
        }
    }
}
